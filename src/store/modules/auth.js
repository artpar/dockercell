import * as types from '../mutation-types'

// initial state
const state = {
  token: null
};

// getters
const getters = {
  token: state => state.token,
  isLoggedIn: state => state.token != null,
};

// actions
const actions = {
  setToken({commit}, token) {
    console.log("set token action", token);
    commit(types.SET_AUTH_TOKEN, token)
  },
  logout({commit}){
    console.log("logout");
    commit(types.LOGOUT);
  }
};

// mutations
const mutations = {
  [types.SET_AUTH_TOKEN](state, token) {
    state.token = token;
    console.log("set token ", token);
    window.localStorage.setItem("token", token);
  },
  [types.LOGOUT](state) {
    state.token = null;
    window.localStorage.removeItem("token");
  },
};

export default {
  state,
  getters,
  actions,
  mutations
}
