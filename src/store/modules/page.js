import * as types from '../mutation-types'


// initial state
const state = {
  title: ""
};

// getters
const getters = {
  title: state => state.title
};

// actions
const actions = {
  setTitle({commit}, title) {
    commit(types.SET_TITLE, {title})
  }
};

// mutations
const mutations = {
  // [types.RECEIVE_PRODUCTS](state, {products}) {
  //   state.all = products
  // },
  //
  [types.SET_TITLE](state, {title}) {
    state.title = title;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
}
