import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import products from './modules/products'
import auth from './modules/auth'
import Vue from "vue";

Vue.use(Vuex);

export default new Vuex.Store({
  actions,
  getters,
  modules: {
    products, auth
  },
})
