const AppConfig = function () {

  const that = this;


  that.apiRoot = window.location.protocol + "//" + window.location.host;

  that.location = {
    protocol: window.location.protocol,
    host: window.location.host,
    hostname: window.location.hostname,
  };

  if (that.location.hostname == "dockercell.com") {
    that.apiRoot = that.location.protocol + "//dockercell.com:8090";
  }

  if (that.location.hostname == "docile.online") {
  that.apiRoot = that.location.protocol + "//api.docile.online";
  }

  that.data = {};

  that.localStorage = {
    getItem: function (key) {
      return that.data[key]
    },
    setItem: function (key, item) {
      that.data[key] = item;
    }
  };

  return that;
}


const appconfig = new AppConfig();


export default appconfig
