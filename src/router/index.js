import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Login from '@/components/Login'
import Register from '@/components/Register'
import MyImages from '@/components/MyImages'
import NewImage from '@/components/NewImage'
import NewPackage from '@/components/NewPackage'
import ListImages from '@/components/ListImages'
import BuyPackage from '@/components/BuyPackage'
import ViewImage from '@/components/ViewImage'
import BuyImage from '@/components/BuyImage'
import Meta from 'vue-meta'

Vue.use(Router);
Vue.use(Meta);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: ListImages
    },
    {
      path: '/auth/login',
      name: "Login",
      component: Login
    },
    {
      path: '/auth/register',
      name: "Register",
      component: Register
    },
    {
      path: '/my/images',
      name: "MyImages",
      component: MyImages,
      meta: {
        requiresAuth: true,
      }

    },
    {
      path: '/images',
      name: "ListImages",
      component: ListImages
    },
    {
      path: '/my/images/new',
      name: "NewImage",
      component: NewImage,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/my/image/:image_id',
      name: "ViewImage",
      component: ViewImage,
      meta: {
        requiresAuth: true,
      }

    },
    {
      path: '/my/image/:image_id/package/new',
      name: "NewPackage",
      component: NewPackage,
      meta: {
        requiresAuth: true,
      }

    },
    {
      path: '/image/:image_name',
      name: "BuyImage",
      component: BuyImage
    },
    {
      path: '/image/:image_name/:package_name',
      name: "BuyPackage",
      component: BuyPackage
    }
  ]
})
