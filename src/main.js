// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import './stylus/main.styl'
import ElementUI, {Notification} from "element-ui";

import App from './App.vue'
import router from './router'
import locale from 'element-ui/lib/locale/lang/en'


Vue.use(Vuetify);
Vue.use(ElementUI, {locale});
Vue.config.productionTip = false;
import store from './store/index.js'
import {getToken} from './api/auth'


router.beforeEach((to, from, next) => {
  console.log("before each", to, from, next);
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!getToken()) {
      window.localStorage.setItem("last", to.matched[0].path);
      next({
        path: '/auth/login',
        query: {
          redirect: to.fullPath,
        },
      });
    } else {
      next();
    }
  } else {
    next();
  }
});


import('flat-social-icons/flat-icons.css');
// import('font-awesome/css/font-awesome.min.css');
require('ionicons/dist/css/ionicons.min.css');
require("element-ui/packages/theme-chalk/lib/index.css");

window.titleCase = function (str) {
  if (!str || !str.replace) {
    return str
  }
  // console.log("TitleCase  : [" + str + "]", str)
  if (!str || str.length < 2) {
    return str;
  }
  let s = str.replace(/[-_]+/g, " ").trim().split(' ')
    .map(w => (w[0] ? w[0].toUpperCase() : "") + w.substr(1).toLowerCase()).join(' ');
  // console.log("titled: ", s);
  return s
};

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {App},
  store: store,
});
