#!/usr/bin/env bash

service=$1
domain=$2
namespace=$3


if [ ! $# -eq 3 ]; then
    echo "Service/namespace name not provided"
    echo "Usage: $0 <service> <domain> <namespace>"
    exit 1
fi


cat <<EOF | kubectl --namespace $namespace apply -f -
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: $service
  namespace: $namespace
  annotations:
    kubernetes.io/tls-acme: "true"
    kubernetes.io/ingress.class: "nginx"
spec:
  tls:
  - hosts:
    - $domain
    secretName: $service-$domain-tls
EOF

