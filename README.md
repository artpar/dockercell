# dockercell

> sell your docker images as a service

## setup on local

> Start the database and api server

```bash
cd backend
./build.sh 
docker-compose up -d
```

- Run the npm dev

```
PORT=7111 npm run dev
```

- point `site.daptin.com` to 127.0.0.1 using /etc/hosts
- point `dockercell.com` to 127.0.0.1 using /etc/hosts

- dockercell.com:8090 -> daptin dashboard
- dockercell.com:7111 -> dockercell frontend (live reload with npm)


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
